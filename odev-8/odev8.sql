/* 1. test veritabanınızda employee isimli sütun bilgileri id(INTEGER), name VARCHAR(50), birthday DATE, email VARCHAR(100) olan bir tablo oluşturalım. */
CREATE TABLE employee {
    id INTEGER NOT NULL PRIMARY KEY ,
    name VARCHAR(50) NOT NULL,
    birthday DATE NOT NULL,
    email VARCHAR(100) NOT NULL
}

/* 2. Oluşturduğumuz employee tablosuna 'Mockaroo' servisini kullanarak 50 adet veri ekleyelim. */
INSERT INTO mytable(id,name,birthday,email) VALUES (1,'Carmela','2/8/2004','cknewstub0@loc.gov');
INSERT INTO mytable(id,name,birthday,email) VALUES (2,'Noelani','4/23/1970','nalbiston1@cisco.com');
INSERT INTO mytable(id,name,birthday,email) VALUES (3,'Fanya','3/27/1998','fgeipel2@elegantthemes.com');
INSERT INTO mytable(id,name,birthday,email) VALUES (4,'Val','6/22/1955','vshrawley3@nih.gov');
INSERT INTO mytable(id,name,birthday,email) VALUES (5,'Wally','5/4/1974','wsclanders4@unblog.fr');
INSERT INTO mytable(id,name,birthday,email) VALUES (6,'Arleyne','8/8/1964','acribbins5@cisco.com');
INSERT INTO mytable(id,name,birthday,email) VALUES (7,'Gustav','9/15/1981','gkingsley6@purevolume.com');
INSERT INTO mytable(id,name,birthday,email) VALUES (8,'Benni','5/3/1978','bmossop7@weebly.com');
INSERT INTO mytable(id,name,birthday,email) VALUES (9,'Betta','7/13/1989','btunesi8@biglobe.ne.jp');
INSERT INTO mytable(id,name,birthday,email) VALUES (10,'Stan','1/26/1972','sorum9@wikispaces.com');
INSERT INTO mytable(id,name,birthday,email) VALUES (11,'Ina','12/17/1981','ibaxa@feedburner.com');
INSERT INTO mytable(id,name,birthday,email) VALUES (12,'Mabelle','5/24/1999','mjadob@cyberchimps.com');
INSERT INTO mytable(id,name,birthday,email) VALUES (13,'Mariska','8/30/1972','mcraykc@lulu.com');
INSERT INTO mytable(id,name,birthday,email) VALUES (14,'Merrill','10/2/1997','mdoubravad@ed.gov');
INSERT INTO mytable(id,name,birthday,email) VALUES (15,'Jami','6/11/1984','jfrenzele@youku.com');
INSERT INTO mytable(id,name,birthday,email) VALUES (16,'Wynn','6/16/1958','wgablef@paginegialle.it');
INSERT INTO mytable(id,name,birthday,email) VALUES (17,'Kayla','3/22/1988','knowlang@apple.com');
INSERT INTO mytable(id,name,birthday,email) VALUES (18,'Wolfgang','7/17/1965','welmsh@homestead.com');
INSERT INTO mytable(id,name,birthday,email) VALUES (19,'Guendolen','9/27/1951','gpanasi@cargocollective.com');
INSERT INTO mytable(id,name,birthday,email) VALUES (20,'Maisie','6/20/1999','mbalassij@cpanel.net');
INSERT INTO mytable(id,name,birthday,email) VALUES (21,'George','8/20/2003','gmaceveleyk@umich.edu');
INSERT INTO mytable(id,name,birthday,email) VALUES (22,'L;urette','8/26/1963','lmcnerlinl@adobe.com');
INSERT INTO mytable(id,name,birthday,email) VALUES (23,'Kipper','7/6/1986','kprowtingm@phoca.cz');
INSERT INTO mytable(id,name,birthday,email) VALUES (24,'Corie','11/16/1987','cbeastalln@qq.com');
INSERT INTO mytable(id,name,birthday,email) VALUES (25,'Millicent','2/7/1981','mtregiano@vinaora.com');
INSERT INTO mytable(id,name,birthday,email) VALUES (26,'Powell','7/5/1960','phalksworthp@huffingtonpost.com');
INSERT INTO mytable(id,name,birthday,email) VALUES (27,'Cecil','4/6/1963','cstonehamq@vkontakte.ru');
INSERT INTO mytable(id,name,birthday,email) VALUES (28,'Thorny','9/18/1983','tcoveyr@blogs.com');
INSERT INTO mytable(id,name,birthday,email) VALUES (29,'Rivkah','7/29/1959','rduffries@about.com');
INSERT INTO mytable(id,name,birthday,email) VALUES (30,'August','2/5/1995','ahanhamt@archive.org');
INSERT INTO mytable(id,name,birthday,email) VALUES (31,'Engelbert','10/4/1977','ederrettu@ow.ly');
INSERT INTO mytable(id,name,birthday,email) VALUES (32,'Vincenz','8/12/1964','vguenyv@scientificamerican.com');
INSERT INTO mytable(id,name,birthday,email) VALUES (33,'Lacey','4/14/1990','llebandw@reverbnation.com');
INSERT INTO mytable(id,name,birthday,email) VALUES (34,'Dore','10/3/1991','dgrebnerx@hc360.com');
INSERT INTO mytable(id,name,birthday,email) VALUES (35,'Yoko','7/21/1981','yyoxally@xing.com');
INSERT INTO mytable(id,name,birthday,email) VALUES (36,'Symon','12/17/1952','shedworthz@nhs.uk');
INSERT INTO mytable(id,name,birthday,email) VALUES (37,'Valenka','7/6/2005','vlamping10@topsy.com');
INSERT INTO mytable(id,name,birthday,email) VALUES (38,'Mercy','8/26/1997','mflynn11@typepad.com');
INSERT INTO mytable(id,name,birthday,email) VALUES (39,'Gusti','7/9/1958','gwinward12@zdnet.com');
INSERT INTO mytable(id,name,birthday,email) VALUES (40,'Marisa','6/19/1972','malvey13@example.com');
INSERT INTO mytable(id,name,birthday,email) VALUES (41,'Poul','4/16/1959','pjemmett14@ovh.net');
INSERT INTO mytable(id,name,birthday,email) VALUES (42,'Durante','5/24/1981','dmalec15@usatoday.com');
INSERT INTO mytable(id,name,birthday,email) VALUES (43,'Theo','5/7/1982','tsimonett16@nasa.gov');
INSERT INTO mytable(id,name,birthday,email) VALUES (44,'Bella','4/9/1965','bdyett17@boston.com');
INSERT INTO mytable(id,name,birthday,email) VALUES (45,'Cordie','11/1/2004','clhomme18@jimdo.com');
INSERT INTO mytable(id,name,birthday,email) VALUES (46,'Tab','12/23/2002','tsavin19@ask.com');
INSERT INTO mytable(id,name,birthday,email) VALUES (47,'Evania','1/20/1956','eturbat1a@meetup.com');
INSERT INTO mytable(id,name,birthday,email) VALUES (48,'Tulley','12/3/1987','tminelli1b@typepad.com');
INSERT INTO mytable(id,name,birthday,email) VALUES (49,'Cobbie','6/21/1957','cwhyte1c@cafepress.com');
INSERT INTO mytable(id,name,birthday,email) VALUES (50,'Annie','5/29/1968','abaxstare1d@usgs.gov'); 

/* 3. Sütunların her birine göre diğer sütunları güncelleyecek 5 adet UPDATE işlemi yapalım. */
UPDATE employee SET name = "Updated Name", birthday ="09/15/2003", email="update@email.com" WHERE id = 2;

UPDATE employee SET name = "Updated Name2", birthday ="08/12/2001", email="update2@email.com" WHERE name LIKE A%;

UPDATE employee SET name = "Updated Name3", birthday ="03/13/1999", email="update3@email.com" WHERE email LIKE %.edu;

UPDATE employee SET name = "Updated Name4", birthday ="04/14/1994", email="update4@email.com" WHERE id BETWEEN 25 AND 45;

UPDATE employee SET name = "Updated Name5", birthday ="05/15/2005", email="update5@email.com" WHERE name LIKE _e%;


/* 4. Sütunların her birine göre ilgili satırı silecek 5 adet DELETE işlemi yapalım. */
DELETE FROM employee WHERE id = 5;

DELETE FROM employee WHERE name LIKE C%;

DELETE FROM employee WHERE email LIKE %.ru;

DELETE FROM employee WHERE id > 40;

DELETE FROM employee WHERE birthday >= '1/1/2004';





